package surface;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;

import data.Data;
import photosave.SavePhoto;
import puzzle.Puzzle;
import puzzle.PuzzleFactory;
import sound.MyMediaPlayer;
import nasa.puzzles.R;
import state.CommonVariables;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.TransitionDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A class to hold the surface of the puzzle and the thread for updating physics
 * and drawing.
 *
 * Currently no splash screen implemented.
 * 
 * @author Rick
 * 
 */
public class PuzzleSurface extends SurfaceView implements
		SurfaceHolder.Callback {

	public static final int EASY = 0;
	public static final int HARD = 1;
	public static final int VERY_HARD = 2;

	public static final int STATE_RESUMING = 0;
	public static final int STATE_LOSE = 1;
	public static final int STATE_PAUSE = 2;
	public static final int STATE_READY = 3;
	public static final int STATE_RUNNING = 4;

	public static final int TRANS_VALUE = (255 / 2);
	public static final int STROKE_VALUE = 5;

	Thread loadingThread;

	Intent intent;
	boolean mExternalStorageAvailable;
	boolean mExternalStorageWriteable;
	Toast toast;

	public TransitionDrawable buttonTrans;

	public Paint borderPaintA;
	public Paint borderPaintB;
	public Paint transPaint;
	public Paint fullPaint;

	PuzzleFactory pf = new PuzzleFactory();
	Puzzle puzzle;
	public PuzzleThread puzzleThread;

	boolean difficultyChanged;

	public MyMediaPlayer myMediaPlayer;
	Handler myHandler;

	CommonVariables cv = CommonVariables.getInstance();

	class IncomingHandlerCallback implements Handler.Callback {
		@Override
		public boolean handleMessage(Message m) {
			// handle message code
			cv.mStatusText.setVisibility(m.getData().getInt("viz"));
			cv.mStatusText.setText(m.getData().getString("text"));
			return true;
		}
	}

	public PuzzleSurface(Context context, AttributeSet attrs) {
		super(context, attrs);
		// set context for access in other classes
		cv.context = context;

		// register our interest in hearing about changes to our surface
		SurfaceHolder holder = getHolder();
		holder.addCallback(this);

		borderPaintA = new Paint();
		borderPaintA.setStyle(Paint.Style.STROKE);
		borderPaintA.setStrokeWidth(STROKE_VALUE);
		borderPaintA.setColor(Color.LTGRAY);
		borderPaintA.setAlpha(TRANS_VALUE);

		borderPaintB = new Paint();
		borderPaintB.setStyle(Paint.Style.STROKE);
		borderPaintB.setStrokeWidth(STROKE_VALUE);
		borderPaintB.setColor(Color.DKGRAY);
		borderPaintB.setAlpha(TRANS_VALUE);

		transPaint = new Paint();
		transPaint.setAlpha(TRANS_VALUE);
		transPaint.setStyle(Paint.Style.FILL);

		fullPaint = new Paint();
		myHandler = new Handler(new IncomingHandlerCallback());

		// create thread only; it's started in surfaceCreated()
		puzzleThread = new PuzzleThread(holder, context, myHandler);

		cv.res = context.getResources();
		cv.rand = new Random();
	}

	/**
	 * Saves a photo in original resolution. Variable current image may change
	 * as the application is used.
	 */
	public void actaullySavePhoto(int currImage) {
		final Integer currentImageToSave = Integer.valueOf(Integer
				.valueOf(currImage));

		Activity activity = (Activity) cv.context;
		activity.runOnUiThread(new Runnable() {

			public void run() {
				// thread to save images, should have no bearing on the rest of
				// the application

				buttonTrans.startTransition(500);

				// save current image to devices images folder
				String state = Environment.getExternalStorageState();
				// check if writing is an option
				if (Environment.MEDIA_MOUNTED.equals(state)) {
					// We can read and write the media
					mExternalStorageAvailable = mExternalStorageWriteable = true;
				} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
					// We can only read the media
					mExternalStorageAvailable = true;
					mExternalStorageWriteable = false;
				} else {
					// Something else is wrong. It may be one of many other
					// states, but
					// all we need
					// to know is we can neither read nor write
					mExternalStorageAvailable = mExternalStorageWriteable = false;
				}

				if (mExternalStorageAvailable && mExternalStorageWriteable) {
					// then write picture to phone
					File path = Environment
							.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

					String name = Data.ARTIST + currentImageToSave + ".jpeg";

					File file = new File(path, name);
					InputStream is = null;

					// check for file in directory
					if (file.exists()) {
						showToast(cv.context, "Photo Exists Already!");
					} else {
						try {
							boolean b1 = path.mkdirs();
							boolean b2 = path.exists();
							// Make sure the Pictures directory exists.
							if (b1 || b2) {

								// get file into input stream
								is = getResources().openRawResource(
										Data.PICS[currentImageToSave]);

								OutputStream os = new FileOutputStream(file);
								byte[] data = new byte[is.available()];
								is.read(data);
								os.write(data);
								is.close();
								os.close();

								showToast(cv.context, "Photo saved");
								cv.mySoundPool.playChimeSound();

								// Tell the media scanner about the new file so
								// that it
								// is
								// immediately available to the user.
								MediaScannerConnection
										.scanFile(
												cv.context,
												new String[] { file.toString() },
												null,
												new MediaScannerConnection.OnScanCompletedListener() {
													@Override
													public void onScanCompleted(
															String path, Uri uri) {
														// Log.i("ExternalStorage",
														// "Scanned " + path +
														// ":");
														// Log.i("ExternalStorage",
														// "-> uri=" + uri);
													}
												});
							} else {
								showToast(cv.context,
										"Could not make/access directory.");
							}
						} catch (IOException e) {
							showToast(cv.context,
									"Error making/accessing directory.");
						}
					}
				} else {
					showToast(cv.context, "Directory not available/writable.");
				}

			}

		});

	}

	public void nextImage() {
		synchronized (puzzleThread.mSurfaceHolder) {
			if (cv.mSaveButton.isShown())
				cv.mSaveButton.setVisibility(INVISIBLE);

			AlertDialog.Builder builder = new AlertDialog.Builder(cv.context);

			builder.setTitle("Radical\u2605Appwards\nSolve Time = "
					+ puzzle.getSolveTime() + " secs.");
			builder.setMessage("Do you want to Save this image?");
			builder.setPositiveButton("Yup",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							new SavePhoto(cv.currentPuzzleImagePosition);
							// actaullySavePhoto(cv.currentImage);
							hideButtons();
							puzzleThread.setState(PuzzleSurface.STATE_PAUSE);
							cv.imageReady = false;
							puzzle.getNewImageLoadedScaledDivided(loadingThread);
							dialog.dismiss();
						}
					});

			builder.setNegativeButton("Nope",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							hideButtons();
							puzzleThread.setState(PuzzleSurface.STATE_PAUSE);
							cv.imageReady = false;
							puzzle.getNewImageLoadedScaledDivided(loadingThread);
							dialog.dismiss();
						}
					});

			builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					// if the dialog is canceled give the option to start it
					// again
					if (!cv.mSaveButton.isShown())
						cv.mSaveButton.setVisibility(VISIBLE);
					if (cv.mStatusText.isShown())
						cv.mStatusText.setVisibility(INVISIBLE);
					puzzleThread.setState(PuzzleSurface.STATE_RUNNING);
					dialog.dismiss();
				}
			});

			AlertDialog alert = builder.create();
			alert.show();
		}
	}

	public void showButtons() {
		if (cv.mSaveButton.getVisibility() == View.INVISIBLE)
			cv.mSaveButton.setVisibility(VISIBLE);
		if (cv.rightWebLinkButton.getVisibility() == View.INVISIBLE)
			cv.rightWebLinkButton.setVisibility(VISIBLE);
		if (cv.leftWebLinkButton.getVisibility() == View.INVISIBLE)
			cv.leftWebLinkButton.setVisibility(VISIBLE);
		if (cv.adView.getVisibility() == View.INVISIBLE)
			cv.adView.setVisibility(VISIBLE);
	}

	public void hideButtons() {
		if (cv.mSaveButton.getVisibility() == View.VISIBLE)
			cv.mSaveButton.setVisibility(INVISIBLE);
		if (cv.rightWebLinkButton.getVisibility() == View.VISIBLE)
			cv.rightWebLinkButton.setVisibility(INVISIBLE);
		if (cv.leftWebLinkButton.getVisibility() == View.VISIBLE)
			cv.leftWebLinkButton.setVisibility(INVISIBLE);
		if (cv.adView.getVisibility() == View.VISIBLE) {
			cv.adView.setVisibility(INVISIBLE);
		}
	}

	public void showToast(Context cont, String message) {
		// create if not, or set text to it
		if (toast == null) {
			toast = Toast.makeText(cont, message, Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.BOTTOM | Gravity.CENTER, 0, 0);
		}
		if (!toast.getView().isShown()) {
			toast.setText(message);
			toast.show();
		} else {
			toast.setText(message);
		}
	}

	public void setButton(Button button) {
		cv.mSaveButton = button;
	}

	public void setBottomRightButton(ImageButton daButton) {
		cv.rightWebLinkButton = daButton;
	}

	public void setBottomLeftButton(ImageButton faButton) {
		cv.leftWebLinkButton = faButton;
	}

	public void setTextView(TextView textView) {
		cv.mStatusText = textView;
	}

	@Override
	public void onWindowFocusChanged(boolean hasWindowFocus) {
		if (!hasWindowFocus) {
			puzzleThread.pause();
		}
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		puzzleThread.setSurfaceSize(width, height);
	}

	public void surfaceCreated(SurfaceHolder holder) {
		// create a switch for the states
		if (puzzleThread != null) {
			switch (puzzleThread.getState()) {
			case BLOCKED:
				// The thread is waiting.
				break;
			case NEW:
				// The thread may be run.
				puzzleThread.start();
				break;
			case RUNNABLE:
				// The thread is blocked and waiting for a lock.
				break;
			case TERMINATED:
				puzzleThread = new PuzzleThread(holder, cv.context, myHandler);
				puzzleThread.start();
				break;
			case TIMED_WAITING:
				// The thread has been terminated.
				break;
			case WAITING:
				// The thread is waiting for a specified amount of time.
				break;
			default:
				break;
			}
			puzzleThread.setRunning(true);
		}
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		boolean retry = true;
		puzzleThread.setRunning(false);
		while (retry) {
			try {
				puzzleThread.join();
				retry = false;
			} catch (InterruptedException e) {
			}
		}
	}

	@Override
	public boolean performClick() {
		super.performClick();
		return true;
	}

	/**
	 * Perform Click is called on UP press to perform accessebility type
	 * actions.
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		synchronized (puzzleThread.getSurfaceHolder()) {
			// implement performClick
			switch (puzzleThread.mMode) {
			case STATE_READY:
				hideButtons();
				puzzleThread.doStart();
				startTimer();
				return false;
			case STATE_PAUSE:
			case STATE_RESUMING:
				if (cv.mStatusText.isShown()) {
					cv.mStatusText.setVisibility(View.INVISIBLE);
					hideButtons();
				}
				puzzleThread.doStart();
				return false;
			case STATE_RUNNING:
				if (cv.solved) {
					// win screen is before the next image button is pressed
					if (cv.mSaveButton.getVisibility() == View.VISIBLE)
						cv.mSaveButton.setVisibility(INVISIBLE);
					else
						cv.mSaveButton.setVisibility(VISIBLE);

					if (cv.rightWebLinkButton.getVisibility() == View.VISIBLE)
						cv.rightWebLinkButton.setVisibility(INVISIBLE);
					else
						cv.rightWebLinkButton.setVisibility(VISIBLE);

					if (cv.leftWebLinkButton.getVisibility() == View.VISIBLE)
						cv.leftWebLinkButton.setVisibility(INVISIBLE);
					else
						cv.leftWebLinkButton.setVisibility(VISIBLE);

					if (cv.adView.getVisibility() == View.VISIBLE)
						cv.adView.setVisibility(INVISIBLE);
					else
						cv.adView.setVisibility(VISIBLE);

					return false;
				} else if (cv.imageReady) {
					// if (event.getAction() == MotionEvent.ACTION_UP) {
					// performClick();
					// }
					return puzzle.onTouchEvent(event);
				}
				break;
			}
			return true;
		}
	}

	public void setCurrPieceOnTouch(int newPiece) {
		cv.currPieceOnTouch = newPiece;
	}

	public void recreatePuzzle() {
		// to recreate hide the UI
		puzzleThread.setState(STATE_PAUSE);
		cv.imageReady = false;
		puzzle = pf.getPuzzle(cv.difficulty);
		puzzle.getNewImageLoadedScaledDivided(loadingThread);
		hideButtons();
	}

	public void solve() {
		for (int i = 0; i < cv.numberOfPieces; i++) {
			int oldslot = 8 - i;
			int newslot = cv.slotOrder[i];
			if (oldslot != newslot) {
				PuzzlePiece temp = new PuzzlePiece();
				temp = cv.puzzleSlots[oldslot].puzzlePiece;
				cv.puzzleSlots[oldslot].puzzlePiece = cv.puzzleSlots[newslot].puzzlePiece;
				cv.puzzleSlots[oldslot].puzzlePiece.px = cv.puzzleSlots[oldslot].sx;
				cv.puzzleSlots[oldslot].puzzlePiece.py = cv.puzzleSlots[oldslot].sy;
				cv.puzzleSlots[newslot].puzzlePiece = temp;
				cv.puzzleSlots[newslot].puzzlePiece.px = cv.puzzleSlots[newslot].sx;
				cv.puzzleSlots[newslot].puzzlePiece.py = cv.puzzleSlots[newslot].sy;
				temp = null;
			}
		}
		cv.solved = true;
	}

	public void devArtActivity() {
		Intent intent2 = new Intent(Intent.ACTION_VIEW);
		intent2.setData(Uri.parse(Data.DEVART_LINK));
		cv.context.startActivity(intent2);
	}

	public void planetActivity() {
		Intent intent1 = new Intent(Intent.ACTION_VIEW);
		intent1.setData(Uri.parse(Data.PIC_URLS[cv.currentPuzzleImagePosition]));
		cv.context.startActivity(intent1);
	}

	public void cleanUp() {
		if (puzzle != null)
			puzzle.recylceAll();
		if (loadingThread != null && loadingThread.isAlive()) {
			loadingThread.interrupt();
			loadingThread = null;
		}
	}

	public void startTimer() {
		// start timer for new puzzle in puzzle
		if (puzzle != null)
			puzzle.initTimer();
	}

	public void pause() {
		// add conditional null checks here
		if (puzzleThread != null)
			puzzleThread.pause();
	}

	public String getSlotString() {
		String s = "";
		for (int i = 0; i < cv.puzzleSlots.length; i++) {
			if (i == 0) {
				s = "" + cv.puzzleSlots[i].puzzlePiece.pieceNum;
			} else {
				s = s + "," + cv.puzzleSlots[i].puzzlePiece.pieceNum;
			}
		}
		return s;
	}

	public void jumblePictureFromOldPuzzle() {
		for (int i = 0; i < cv.numberOfPieces; i++) {
			int oldslot = i;
			int newslot = cv.slotOrder[i];
			if (oldslot != newslot) {
				PuzzlePiece temp = new PuzzlePiece();
				temp = cv.puzzleSlots[oldslot].puzzlePiece;
				cv.puzzleSlots[oldslot].puzzlePiece = cv.puzzleSlots[newslot].puzzlePiece;
				cv.puzzleSlots[oldslot].puzzlePiece.px = cv.puzzleSlots[oldslot].sx;
				cv.puzzleSlots[oldslot].puzzlePiece.py = cv.puzzleSlots[oldslot].sy;
				cv.puzzleSlots[newslot].puzzlePiece = temp;
				cv.puzzleSlots[newslot].puzzlePiece.px = cv.puzzleSlots[newslot].sx;
				cv.puzzleSlots[newslot].puzzlePiece.py = cv.puzzleSlots[newslot].sy;
				temp = null;
			}
		}
	}

	public class PuzzleThread extends Thread {

		public boolean mRun = false;
		public SurfaceHolder mSurfaceHolder;
		public int mMode;
		public Handler mHandler;

		/**
		 * Locking object for the thread.
		 */
		Object mRunLock = new Object();

		public PuzzleThread(SurfaceHolder surfaceHolder, Context context,
				Handler handler) {
			// get handles to some important objects such as the context which
			// will be updated to continue the threadf
			mSurfaceHolder = surfaceHolder;
			mHandler = handler;
			cv.context = context;
		}

		@Override
		public void run() {
			while (mRun) {
				Canvas c = null;
				try {
					c = mSurfaceHolder.lockCanvas(null);
					synchronized (mSurfaceHolder) {
						if (mMode == STATE_RUNNING)
							updatePhysics();
						synchronized (mRunLock) {
							if (mRun)
								doDraw(c);
						}
					}
				} finally {
					if (c != null) {
						mSurfaceHolder.unlockCanvasAndPost(c);
					}
				}
			}
		}

		public void setRunning(boolean b) {
			mRun = b;
		}

		//
		public SurfaceHolder getSurfaceHolder() {
			return mSurfaceHolder;
		}

		public void updatePhysics() {
			if (difficultyChanged) {
				difficultyChanged = false;
				recreatePuzzle();
			}
		}

		public void doDraw(Canvas canvas) {
			if (canvas != null) {
				if (puzzle == null) {
					if (cv.resumePreviousPuzzle) {
						puzzle = pf.getPuzzle(cv.difficulty);
						cv.imageReady = false;
						puzzle.getPrevousImageLoadedScaledDivided(loadingThread);
						puzzle.initTimer();
					} else if (difficultyChanged) {
						difficultyChanged = false;
						recreatePuzzle();
					} else {
						// start with an easy puzzle
						puzzle = pf.getPuzzle(EASY);
						cv.imageReady = false;
						puzzle.getNewImageLoadedScaledDivided(loadingThread);
						puzzle.initTimer();
					}
				} else if (cv.imageReady) {

					canvas.drawColor(Color.BLACK);

					// draw a moving piece away from its original location
					if (cv.movingPiece) {
						for (int i = 0; i < cv.numberOfPieces; i++) {
							// draw pieces
							if (!cv.puzzleSlots[i].puzzlePiece.bitmap
									.isRecycled() && cv.currPieceOnTouch != i)
								canvas.drawBitmap(
										cv.puzzleSlots[i].puzzlePiece.bitmap,
										cv.puzzleSlots[i].puzzlePiece.px,
										cv.puzzleSlots[i].puzzlePiece.py, null);
							// draw border to pieces
							if (!cv.solved && cv.drawBorders)
								canvas.drawRect(
										cv.puzzleSlots[i].sx,
										cv.puzzleSlots[i].sy,
										cv.puzzleSlots[i].sx
												+ cv.puzzleSlots[i].puzzlePiece.bitmap
														.getWidth(),
										cv.puzzleSlots[i].sy
												+ cv.puzzleSlots[i].puzzlePiece.bitmap
														.getHeight(),
										borderPaintA);
						}

						// draw the actual piece
						if (!cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.bitmap
								.isRecycled()) {

							// draw moving image in original location
							canvas.drawBitmap(
									cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.bitmap,
									cv.puzzleSlots[cv.currPieceOnTouch].sx,
									cv.puzzleSlots[cv.currPieceOnTouch].sy,
									transPaint);

							// draw border around original piece location
							canvas.drawRect(
									cv.puzzleSlots[cv.currPieceOnTouch].sx,
									cv.puzzleSlots[cv.currPieceOnTouch].sy,
									cv.puzzleSlots[cv.currPieceOnTouch].sx
											+ cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.bitmap
													.getWidth(),
									cv.puzzleSlots[cv.currPieceOnTouch].sy
											+ cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.bitmap
													.getHeight(), borderPaintB);

							// draw moving piece
							canvas.drawBitmap(
									cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.bitmap,
									cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.px,
									cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.py,
									fullPaint);

							// draw border around moving piece
							if (cv.drawBorders)
								canvas.drawRect(
										cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.px
												+ (STROKE_VALUE / 2),
										cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.py
												+ (STROKE_VALUE / 2),
										cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.px
												+ cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.bitmap
														.getWidth()
												- (STROKE_VALUE / 2),
										cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.py
												+ cv.puzzleSlots[cv.currPieceOnTouch].puzzlePiece.bitmap
														.getHeight()
												- (STROKE_VALUE / 2),
										borderPaintA);
						}
					} else {
						for (int i = 0; i < cv.numberOfPieces; i++) {
							if (!cv.puzzleSlots[i].puzzlePiece.bitmap
									.isRecycled()) {
								// draw pieces
								canvas.drawBitmap(
										cv.puzzleSlots[i].puzzlePiece.bitmap,
										cv.puzzleSlots[i].puzzlePiece.px,
										cv.puzzleSlots[i].puzzlePiece.py, null);
								// draw borders
								if (!cv.solved && cv.drawBorders)
									canvas.drawRect(
											cv.puzzleSlots[i].sx,
											cv.puzzleSlots[i].sy,
											cv.puzzleSlots[i].sx
													+ cv.puzzleSlots[i].puzzlePiece.bitmap
															.getWidth(),
											cv.puzzleSlots[i].sy
													+ cv.puzzleSlots[i].puzzlePiece.bitmap
															.getHeight(),
											borderPaintA);
							}
						}
					}
				} else {
					// the image is loading
					if (cv.errorLoading) {
						canvas.drawColor(Color.RED);
					} else {
						// draw different colors for
						canvas.drawColor(Color.BLACK);
					}
				}
			}
		}

		public void toggleSetSound() {
			synchronized (mSurfaceHolder) {
				if (cv.playTapSound) {
					cv.playTapSound = false;
					showToast(cv.context, "Set Effect Off");
				} else {
					cv.playTapSound = true;
					showToast(cv.context, "Set Effect On");
				}
			}
		}

		public void toggleBorder() {
			synchronized (mSurfaceHolder) {
				if (cv.drawBorders) {
					cv.drawBorders = false;
					showToast(cv.context, "Borders Off");
				} else {
					cv.drawBorders = true;
					showToast(cv.context, "Borders On");
				}
			}
		}

		public void toggleWinSound() {
			synchronized (mSurfaceHolder) {
				if (cv.playChimeSound) {
					cv.playChimeSound = false;
					showToast(cv.context, "Win Effect Off");
				} else {
					cv.playChimeSound = true;
					showToast(cv.context, "Win Effect On");
				}
			}
		}

		public void toggleMusic() {
			synchronized (mSurfaceHolder) {
				if (cv.playMusic) {
					cv.playMusic = false;
					myMediaPlayer.pause();
					showToast(cv.context, "Music Off");
				} else {
					cv.playMusic = true;
					myMediaPlayer.resume();
					showToast(cv.context, "Music On");
				}
			}
		}

		public void setState(int mode) {
			synchronized (mSurfaceHolder) {
				setState(mode, null);
			}
		}

		public void setState(int mode, CharSequence message) {
			synchronized (mSurfaceHolder) {
				mMode = mode;

				if (mMode == STATE_RUNNING) {
					Message msg = mHandler.obtainMessage();
					Bundle b = new Bundle();
					b.putString("text", "");
					b.putInt("viz", View.INVISIBLE);
					msg.setData(b);
					mHandler.sendMessage(msg);
				} else {
					Resources res = cv.context.getResources();
					CharSequence str = "";
					if (mMode == STATE_READY)
						str = res.getText(R.string.mode_ready);
					else if (mMode == STATE_PAUSE)
						str = res.getText(R.string.mode_pause);
					else if (mMode == STATE_LOSE)
						str = res.getText(R.string.mode_lose);

					if (message != null) {
						str = message + "\n" + str;
					}

					Message msg = mHandler.obtainMessage();
					Bundle b = new Bundle();
					b.putString("text", str.toString());
					b.putInt("viz", View.VISIBLE);
					msg.setData(b);
					mHandler.sendMessage(msg);
				}
			}
		}

		public void setSurfaceSize(int width, int height) {
			// synchronized to make sure these all change atomically
			synchronized (mSurfaceHolder) {
				cv.screenW = width;
				cv.screenH = height;
			}
		}

		public void doStart() {
			synchronized (mSurfaceHolder) {
				// First set the game for Medium difficulty

				// Adjust difficulty for EASY/HARD
				if (cv.difficulty == EASY) {

				} else if (cv.difficulty == HARD) {

				} else if (cv.difficulty == VERY_HARD) {

				}

				setState(STATE_RUNNING);
			}
		}

		public void pause() {
			synchronized (mSurfaceHolder) {
				puzzle.pause();
				setState(STATE_PAUSE);
			}
		}

		public void setDifficulty(int diff) {
			synchronized (mSurfaceHolder) {
				// should be start of application or during a thread's life
				// loading a bitmap and splitting it
				if (!cv.imageReady)
					showToast(cv.context,
							"Please wait for current image to load first.");
				else {
					if (diff != cv.difficulty) {
						// show toast of new difficulty
						if (diff == 0)
							showToast(cv.context, "Easy 3 x 3");
						else if (diff == 1)
							showToast(cv.context, "Hard 4 x 4");
						else if (diff == 2)
							showToast(cv.context, "Very Hard 5 x 5");
						cv.difficulty = diff;
						// difficultyChanged = true;
						recreatePuzzle();
					}
				}
			}
		}

		public Bundle saveState(Bundle map) {
			synchronized (mSurfaceHolder) {
				if (map != null) {
					// map.putInt("difficulty", cv.difficulty);
				}
			}
			return map;
		}
	}

}