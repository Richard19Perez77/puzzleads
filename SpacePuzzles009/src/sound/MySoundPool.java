package sound;

import nasa.puzzles.R;
import state.CommonVariables;
import android.media.SoundPool;

/**
 * 
 * Sound Pool extension that included loading sound and playing sounds.
 * 
 * @author Rick
 *
 */
public class MySoundPool extends SoundPool {

	CommonVariables cv = CommonVariables.getInstance();

	public MySoundPool(int maxStreams, int streamType, int srcQuality) {
		super(maxStreams, streamType, srcQuality);
		// create a new sound pool and set up sounds
		this.setOnLoadCompleteListener(new OnLoadCompleteListener() {
			public void onLoadComplete(SoundPool soundPool, int sampleId,
					int status) {

				if (sampleId == 1)
					cv.tapLoaded = true;
				else if (sampleId == 2)
					cv.chimeLoaded = true;

			}
		});

		cv.saveSound = load(cv.context, R.raw.chime, 1);
		cv.tapSound = load(cv.context, R.raw.tap01, 1);
	}

	public void playChimeSound() {
		// check for sound file to be loaded and wanting to be player
		if (cv.chimeLoaded && cv.playChimeSound) {
			play(cv.saveSound, cv.volume, cv.volume, 1, 0, 1f);
		}
	}

	public void playSetSound() {
		// check for tap sound to be loaded and it in preferences
		if (cv.tapLoaded && cv.playTapSound) {
			play(cv.tapSound, cv.volume, cv.volume, 1, 0, 1f);
		}
	}
}